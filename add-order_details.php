<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
	header("location: login.php");
	exit;
}
if (isset($_REQUEST['rid']) and $_REQUEST['rid'] != "") {
	$order_id = $_REQUEST['rid'];
}
 include_once('config.php');
$table = "order_details";
if (isset($_REQUEST['submit']) and $_REQUEST['submit'] != "") {
	extract($_REQUEST);
	if ($item_name == "") {
		header('location:' . $_SERVER['PHP_SELF'] . '?msg=un');
		exit;
	} else {
		$DataCount	=	$db->getQueryCount($table, 'id');
		if ($DataCount[0]['total'] < 200) {
			$data	=	array(
				'order_id' => $_REQUEST['rid'],
				'item_id' => $item_name,
				'unit_price' => $unit_price,
				'qty' => $qty,
				'updated_by_user_id' => $_SESSION["id"]
			);
			$insert	=	$db->insert($table, $data);

			if ($insert) {
				header('location:browse-'.$table.'.php?rid='.$order_id.'&msg=ras');
				exit;
			} else {
				header('location:browse-'.$table.'.php?rid='.$order_id.'&msg=rna');
				exit;
			}
		} else {
			header('location:' . $_SERVER['PHP_SELF'] . '?msg=dsd');
			exit;
		}
	}
}
$ItemNameList = $db->getAllItemNames();
$iname = "";
?>

<!doctype html>

<html lang="en-US" xmlns:fb="https://www.facebook.com/2008/fbml" xmlns:addthis="https://www.addthis.com/help/api-spec" prefix="og: http://ogp.me/ns#" class="no-js">

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>starbeacon - Inventory System</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<script src="https://use.fontawesome.com/4102c26c2b.js"></script>

<!-- Custom fonts for this template-->
<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="css/sb-admin-2.min.css" rel="stylesheet">

<body>


	<!-- Page Wrapper -->
	<div id="wrapper">


		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<div class="bg-light border-bottom shadow-sm sticky-top">
					<div class="container">
						<header>
						</header>
					</div>
					<!--/.container-->
				</div>

				<div class="container-fluid">
					<h1 class="h3 mb-0 text-gray-800" style="padding-top: 20px;padding-bottom: 10px;">Receipt Details</h1>

					<?php

					if (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "un") {

						echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> '.ucfirst($table).' name is mandatory field!</div>';
					} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "ras") {

						echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Record added successfully!</div>';
					} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "rna") {

						echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Record not added <strong>Please try again!</strong></div>';
					} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "dsd") {

						echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Please delete a '.$table.' and then try again <strong>We set limit for security reasons!</strong></div>';
					}
					$order_no = $db->getReceiptNo($order_id);

					?>

					<div class="card">

						<div class="card-header"><i class="fa fa-fw fa-plus-circle"></i> <strong>Add Order Detail - Order No: <?php echo $order_no;?></strong> <a href="browse-<?php echo $table ?>.php?rid=<?php echo $_REQUEST['rid'];?>" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-globe"></i> Browse Order Detail</a></div>

						<div class="card-body">



							<div class="col-sm-6">

								<h5 class="card-title">Fields with <span class="text-danger">*</span> are mandatory!</h5>

								<form method="post">

								<div class="form-group">
										<label>Item Name<span class="text-danger">*</span></label>
										<select name="item_name" id="item_name" class="form-control" required>
											<option value=""></option>
											<?php
											$s	=	'';
											foreach ($ItemNameList as $val) {
												$s++;
												?>
												<?php
												$item_selected = "";

												echo "<option value='" . $val['id'] . "'>" . $val['item_name'] . "</option>" ?>
											<?php } ?>
										</select>
									</div>
									<div class="form-group">
										<label>Unit Price<span class="text-danger">*</span></label>
										<input type="text" name="unit_price" id="unit_price" class="form-control" required>
									</div>
									<div class="form-group">
										<label>Qty<span class="text-danger">*</span></label>
										<input type="text" name="qty" id="qty" class="form-control" required>

									</div>



									<div class="form-group">

										<button type="submit" name="submit" value="submit" id="submit" class="btn btn-primary"><i class="fa fa-fw fa-plus-circle"></i> Add Order Details</button>

									</div>

								</form>

							</div>

						</div>

					</div>

				</div>

			</div>
			<!-- End of Main Content -->

			<!-- Footer -->
			<footer class="sticky-footer bg-white">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Copyright &copy; starbeacon 2019</span>
					</div>
				</div>
			</footer>
			<!-- End of Footer -->

		</div>
		<!-- End of Content Wrapper -->

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
	</div>

</body>

</html>