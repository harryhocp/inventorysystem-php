<?php
include_once('config.php');

session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
	header("location: login.php");
	exit;
}

if (isset($_POST["import"])) {
	$filename = $_FILES["file"]["tmp_name"];

	if ($_FILES["file"]["size"] > 0) {
		$file = fopen($filename, "r");
		$inbound_id = $_REQUEST['rid'];
		$int = 0;
		$no_of_record_inserted = 0;
		$no_of_record_error = 0;

		while (($getData = fgetcsv($file, 40000, ",")) !== FALSE) {
			// $sql = "INSERT into  (emp_id,firstname,lastname,email,reg_date) 
			//    values ('" . $getData[0] . "','" . $getData[1] . "','" . $getData[2] . "','" . $getData[3] . "','" . $getData[4] . "')";
			// $result = mysqli_query($con, $sql);
			// Skip header
			if ($int == 0) {
				$int = 1;
			} else {
				$data	=	array(
					'inbound_id' => $_REQUEST['rid'],
					'item_id' => $db->getItemID($getData[0]),
					'box_no' => $getData[1],
					'serial_no' => $getData[2],
					'case_no' => $getData[3],
					'qty' => $getData[4],
					'location_id' => $db->getLocationID($getData[5]),
					'updated_by_user_id' => $_SESSION["id"]
				);
				

				if($db->getItemID($getData[0]) > 0 && $db->CheckLatestStatus($getData[2]) != "Inbound")
				{
				$no_of_record_inserted = $no_of_record_inserted + 1;
				$insert	=	$db->insert('inbound_details', $data);

								//update item histories
								$inbound_detail_id = $db->getLatestInboundDetailID();
								$data2	=	array(
								'inbound_detail_id' => $inbound_detail_id,
								'status' => 'Inbound',
								'updated_by_user_id' => $_SESSION["id"]
								);
				$insert2	=	$db->insert('item_histories', $data2);
				} else {
				$no_of_record_error = $no_of_record_error + 1;
				}
			}
		}
		fclose($file);

		if (!isset($insert)) {
			header('location: browse-inbound_details.php?rid=' . $_REQUEST['rid'] . '&msg=rif&nod='.$no_of_record_inserted.'&noe='.$no_of_record_error);
			exit; 
		} else {
			header('location: browse-inbound_details.php?rid=' . $_REQUEST['rid'] . '&msg=ris&nod='.$no_of_record_inserted.'&noe='.$no_of_record_error);
			exit; 
		}
	}
}


?>


<html>

<head>
	<title></title>

	<head>

	<body>
		<form method='POST' enctype='multipart/form-data'>
			Upload CSV FILE: <input type='file' name='file' id='file' accept=".csv" /> <input type='submit' name='import' value='Import' />
		</form>
		<div>
			Import CSV Format Sample: <a href="inbound_details_import.csv" download="inbound_details_import_template.csv">DOWNLOAD TEMPLATE</a>

		<p><img src="img/import_inbound_details_example.png" width="1022" height="221" alt="" />
		</div>
	</body>

</html>