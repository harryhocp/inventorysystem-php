<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
	header("location: login.php");
	exit;
}
if (isset($_REQUEST['rid']) and $_REQUEST['rid'] != "") {
	$inbound_id = $_REQUEST['rid'];
}
if (isset($_SESSION['beaconsn']) and $_SESSION['beaconsn'] != "") {
	$BeaconSN = $_SESSION['beaconsn'];
} else {
	$BeaconSN = "";
}

include_once('config.php');
$table = "inbound_details";
if (isset($_REQUEST['submit']) and $_REQUEST['submit'] != "") {
	extract($_REQUEST);

	if ($item_name == "") {
		header('location:' . $_SERVER['PHP_SELF'] . '?msg=un');
		exit;
	} else {
		if ($db->CheckSerialEnable($item_name) == 1 ){
			//check item's latest status is not in Inbound **** && $db->CheckSNExist($table, $serial_no) != ""
			if ($db->CheckLatestStatus($serial_no) != "Inbound") {
				$data	=	array(
					'inbound_id' => $_REQUEST['rid'],
					'item_id' => $item_name,
					'box_no' => $box_no,
					'serial_no' => $serial_no,
					'case_no' => $case_no,
					'qty' => $qty,
					'location_id' => $location_code,
					'updated_by_user_id' => $_SESSION["id"]
				);
				$insert	=	$db->insert($table, $data);

				$inbound_detail_id = $db->getLatestInboundDetailID();

				//update item histories
				$data2	=	array(
					'inbound_detail_id' => $inbound_detail_id,
					'status' => 'Inbound',
					'updated_by_user_id' => $_SESSION["id"]
				);
				$insert2	=	$db->insert('item_histories', $data2);

				//store SN into Session if it is SerialEnabled
				$serial_enable = $db->CheckSerialEnable($item_name);
				if ($serial_enable == "1")
					$_SESSION["beaconsn"] = $serial_no;

				//check if multiadd
				if (isset($_POST['multiadd']))
					$multiaddchecked = $_POST['multiadd'];
				else
					$multiaddchecked = 'false';

				if ($insert and $insert2 and $multiaddchecked == 'false') {
					header('location:browse-' . $table . '.php?rid=' . $inbound_id . '&msg=ras');
					exit;
				} elseif ($insert and $insert2 and $multiaddchecked == 'true') {
					// Store value into session
					$_SESSION['item_name'] = $item_name;
					$_SESSION['box_no'] = $box_no;
					$_SESSION['location_code'] = $location_code;

					header('location:add-inbound_details.php?rid=' . $inbound_id . '&msg=sns&multiadd=' . $_POST['multiadd']);
					exit;
				} else {
					header('location:browse-' . $table . '.php?rid=' . $inbound_id . '&msg=rna');
					exit;
				}
			//when serial no is duplicated
			} else if ($db->CheckSNExist($table, $serial_no) != "")
			{
				if ($db->CheckLatestStatus($serial_no) == "Inbound" && $db->CheckSerialEnable($item_name) == 1)
				{
					$_SESSION["beaconsn"] = $serial_no;
					$_SESSION['item_name'] = $item_name;
					$_SESSION['box_no'] = $box_no;
					$_SESSION['location_code'] = $location_code;
					if (isset($_POST['multiadd']))
					header('location:add-inbound_details.php?rid=' . $inbound_id . '&msg=snd&multiadd=true');
					else
					header('location:add-inbound_details.php?rid=' . $inbound_id . '&msg=snd');
					exit;
				}
			}
		//when items is not serial enabeld
		} else if ($db->CheckSerialEnable($item_name) != 1)
		{
			
			$data	=	array(
				'inbound_id' => $_REQUEST['rid'],
				'item_id' => $item_name,
				'box_no' => $box_no,
				'serial_no' => $serial_no,
				'case_no' => $case_no,
				'qty' => $qty,
				'location_id' => $location_code,
				'updated_by_user_id' => $_SESSION["id"]
			);
			$insert	=	$db->insert($table, $data);

			$inbound_detail_id = $db->getLatestInboundDetailID();

			//update item histories
			$data2	=	array(
				'inbound_detail_id' => $inbound_detail_id,
				'status' => 'Inbound',
				'updated_by_user_id' => $_SESSION["id"]
			);
			$insert2	=	$db->insert('item_histories', $data2);

			//store SN into Session if it is SerialEnabled
			$serial_enable = $db->CheckSerialEnable($item_name);
			if ($serial_enable == "1")
				$_SESSION["beaconsn"] = $serial_no;

			//check if multiadd
			if (isset($_POST['multiadd']))
				$multiaddchecked = $_POST['multiadd'];
			else
				$multiaddchecked = 'false';

			if ($insert and $insert2 and $multiaddchecked == 'false') {
				header('location:browse-' . $table . '.php?rid=' . $inbound_id . '&msg=ras');
				exit;
			} elseif ($insert and $insert2 and $multiaddchecked == 'true') {
				// Store value into session
				$_SESSION['item_name'] = $item_name;
				$_SESSION['box_no'] = $box_no;
				$_SESSION['location_code'] = $location_code;

				header('location:add-inbound_details.php?rid=' . $inbound_id . '&msg=sns&multiadd=' . $_POST['multiadd']);
				exit;
			} else {
				header('location:browse-' . $table . '.php?rid=' . $inbound_id . '&msg=rna');
				exit;
			}
		}
	}
}
$RecieptItemNameList = $db->getRecieptItemNamesByInboundID($inbound_id);
$iname = "";
$LocationList = $db->getAllLocations();

?>

<!doctype html>

<html lang="en-US" xmlns:fb="https://www.facebook.com/2008/fbml" xmlns:addthis="https://www.addthis.com/help/api-spec" prefix="og: http://ogp.me/ns#" class="no-js">

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>starbeacon - Inventory System</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<script src="https://use.fontawesome.com/4102c26c2b.js"></script>

<!-- Custom fonts for this template-->
<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="css/sb-admin-2.min.css" rel="stylesheet">

<body>


	<!-- Page Wrapper -->
	<div id="wrapper">


		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<div class="bg-light border-bottom shadow-sm sticky-top">
					<div class="container">
						<header>
						</header>
					</div>
					<!--/.container-->
				</div>

				<div class="container-fluid">
					<h1 class="h3 mb-0 text-gray-800" style="padding-top: 20px;padding-bottom: 10px;">Inbound Details</h1>

					<?php

					if (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "un") {

						echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> ' . ucfirst($table) . ' name is mandatory field!</div>';
					} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "ras") {

						echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Record added successfully!</div>';
					} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "rna") {

						echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Record not added <strong>Please try again!</strong></div>';
					} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "dsd") {

						echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Please delete a ' . $table . ' and then try again <strong>We set limit for security reasons!</strong></div>';
					} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "sns") {

						echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Item SN:' . $BeaconSN . ' added successfully!</strong></div>';
					} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "snd") {

						echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Item SN:' . $BeaconSN . ' is already in inbound status!</strong></div>';
					}

					?>

					<div class="card">
						<div class="card-header"><i class="fa fa-fw fa-plus-circle"></i> <strong>Add Inbound Detail - Inbound ID: <?php echo $_REQUEST['rid']; ?></strong> <a href="browse-<?php echo $table ?>.php?rid=<?php echo $inbound_id ?>" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-globe"></i> Browse Inbound Detail</a></div>
						<div class="card-body">
							<div class="col-sm-6">
								<h5 class="card-title">Fields with <span class="text-danger">*</span> are mandatory!</h5>
								<form method="post">
									<div class="form-group">
										<label>Item Name<span class="text-danger">*</span></label>
										<select name="item_name" id="item_name" class="form-control" onchange="requireSN()" required>
											<option value=""></option>
											<?php
											$s	=	'';
											foreach ($RecieptItemNameList as $val) {
												$s++;
												?>
												<?php
												echo "<option value='" . $val['id'] . "'>" . $val['item_name'] . "</option>" ?>
											<?php } ?>
										</select>
									</div>
									<div class="form-group">
										<label>Box No<span class="text-danger">*</span></label>
										<input type="text" name="box_no" id="box_no" class="form-control" required>
									</div>
									<div class="form-group">
										<label id="snlabel">Serial No</label>
										<input type="text" name="serial_no" id="serial_no" class="form-control">
										<label><input type="checkbox" id="multiadd" name="multiadd" value="true">Multi-Add</label>
									</div>
									<div class="form-group">
										<label>Case No<span class="text-danger"></label>
										<input type="text" name="case_no" id="case_no" class="form-control">
									</div>
									<div class="form-group">
										<label>Qty<span class="text-danger">*</span></label>
										<input type="text" name="qty" id="qty" class="form-control" required>
									</div>	
									<div class="form-group">
										<label>Location Code<span class="text-danger">*</span></label>
										<select name="location_code" id="location_code" class="form-control" required>
											<option value=""></option>
											<?php
											$s	=	'';
											foreach ($LocationList as $val) {
												$s++;
												?>
												<?php
												echo "<option value='" . $val['id'] . "'>" . $val['location_code'] . "</option>" ?>
											<?php } ?>
										</select>
									</div>

									<div class="form-group">
										<button type="submit" name="submit" value="submit" id="submit" class="btn btn-primary"><i class="fa fa-fw fa-plus-circle"></i> Add Inbound Details</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End of Main Content -->

			<!-- Footer -->
			<footer class="sticky-footer bg-white">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Copyright &copy; starbeacon 2019</span>
					</div>
				</div>
			</footer>
			<!-- End of Footer -->

		</div>
		<!-- End of Content Wrapper -->

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

		<!-- JS section -->
		<script>
			//Auto fill Qty if SN required
			function requireSN() {
				var e = document.getElementById("item_name");
				var evalue = e.options[e.selectedIndex].value;
				if (evalue == 1 || evalue == 2) {
					var x = document.getElementById("qty");
					x.value = "1";
					x.readOnly = true;

					//set SN as required
					document.getElementById("serial_no").required = true;
					document.getElementById("snlabel").innerHTML = "Serial No<span class='text-danger'>*</span>";

				} else {
					var x = document.getElementById("qty");
					x.value = "";
					x.readOnly = false;

					//set SN as not required
					document.getElementById("serial_no").required = false;
					document.getElementById("snlabel").innerHTML = "Serial No";

				}
			}

			function AutoFocusSN() {
				var x = document.getElementById("serial_no");
				x.focus();

				//Keep Multiadd Checked
				var y = document.getElementById("multiadd");
				y.checked = true;
			}

			function AutoFillValue() {
				document.getElementById("item_name").value = "<?php echo isset($_SESSION['item_name']) ?>";
				requireSN();
				document.getElementById("box_no").value = "<?php echo isset($_SESSION['box_no']) ?>";
				document.getElementById("location_code").value = "<?php echo isset($_SESSION['location_code']) ?>";
			}
		</script>
	</div>

</body>

</html>

<?php
//Auto focus if multi-add
if (isset($_REQUEST['multiadd']) and $_REQUEST['multiadd'] == "true") {
	echo "<script>AutoFocusSN();</script>";
	//Auto Fill Value
	echo "<script>AutoFillValue();</script>";
}
echo "<script>requireSN();</script>";
?>