<?php
include_once('config.php');

session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
	header("location: login.php");
	exit;
}

if (isset($_REQUEST['Id']) and $_REQUEST['Id'] != "") {
	if (isset($_REQUEST['action'])) {

		$action = $_REQUEST['action'];
		if ($action == "deactivate") {
			$db->deactivate($_REQUEST['target'], array('id' => $_REQUEST['Id']));
			if (isset($_REQUEST['rid']) and $_REQUEST['rid'] != "") {
				header('location: browse-' . $_REQUEST['target'] . '.php?rid=' . $_REQUEST['rid'] . '&msg=rds');
			} else {
				header('location: browse-' . $_REQUEST['target'] . '.php?msg=rds');
			}
		} else {
			$db->activate($_REQUEST['target'], array('id' => $_REQUEST['Id']));
			if (isset($_REQUEST['rid']) and $_REQUEST['rid'] != "") {
				header('location: browse-' . $_REQUEST['target'] . '.php?rid=' . $_REQUEST['rid'] . '&msg=ras');
			} else {
				header('location: browse-' . $_REQUEST['target'] . '.php?msg=ras');
			}
		}
	}
	exit;
}
?>
