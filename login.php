<?php
// Initialize the session
session_start();
 
// Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: index.php");
    exit;
}
 
// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$username = $password = "";
$username_err = $password_err = "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Check if username is empty
    if(empty(trim($_POST["username"]))){
        $username_err = "Please enter username.";
    } else{
        $username = trim($_POST["username"]);
    }
    
    // Check if password is empty
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter your password.";
    } else{
        $password = trim($_POST["password"]);
    }

    $hashed_password = $db->GetUserPassword($username);
    // Validate credentials
                // Check if username exists, if yes then verify password
                if($hashed_password != null){                    
                    // Bind result variables
                        if($db->HashPassword($password) == $hashed_password){
                            // Password is correct, so start a new session
                            session_start();
                            
                            // Store data in session variables
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $db->GetUserID($username);
                            $_SESSION["username"] = $username;
                            $_SESSION["user_role_id"] = $user_role_id;                            
                            // Redirect user to welcome page
                            header("location: index.php");
                        } else{
                            // Display an error message if password is not valid
                            $password_err = "The password you entered was not valid.";
                        }
                } else{
                    // Display an error message if username doesn't exist
                    $username_err = "No account found with that username.";
                }
            } 
?>
 
<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <title>Inventory System</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif;
		text-align: center;}
        /* .wrapper{ width: 360px; padding: 20px;} */
        .wrapper{margin: auto; width: fit-content;}
		.wrapper_text {margin: auto; width: fit-content;}
    </style>
</head>
<body>
	<div class="wrapper">
        <h2><img src="img/starbeacon_logo_op_blue-03.png" width="360" height="87" alt=""/></h2>
		<div class="wrapper_text">
        <h2><b>Inventory System</b></h2>
        <h2>Login</h2>
        <p>Please fill in your credentials to login</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                <label>Username</label>
                <input type="text" name="username" class="form-control" value="<?php echo $username; ?>" require>
                <span class="help-block"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>"require>
                <label>Password</label>
                <input type="password" name="password" class="form-control">
                <span class="help-block"><?php echo $password_err; ?></span>

            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Login">
			</div>
            </div>
        </form>
		</div>
   </div> 
</body>
</html>