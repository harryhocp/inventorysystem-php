<?php
require('fpdf.php');

// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
	header("location: login.php");
	exit;
}

include_once('config.php');
$table = 'outbound_details';
$data;
$details;
$serialnolist;

// Get Data
if (isset($_REQUEST['outbound_id']) and $_REQUEST['outbound_id'] != "") {
    $data	=	$db->getDNItems($_REQUEST['outbound_id']);
    $details    =   $db->getDNDetails($_REQUEST['outbound_id']);
    $order_no = $details[0]['order_no'];
    $order_date = $details[0]['order_date'];
    $cust_id = $details[0]['cust_id'];
    $cust_name = $details[0]['cust_name'];
    $dn_no = "DN".date("Ymd")."-".$_REQUEST['outbound_id'];
    $cust_addr = $details[0]['cust_addr'];
    $cust_contact_person = $details[0]['cust_contact_person'];
    $cust_tel = $details[0]['cust_tel'];
    $delivery_date = $details[0]['delivery_date'];


    $serialnolist = $db->getDNSerialNo($_REQUEST['outbound_id']);
}
else
{
    header('location: browse-outbounds.php?msg=dnr');
    exit;
}

class PDF extends FPDF
{
    // Page header
    function Header()
    {

        global $order_date, $order_no, $dn_no, $cust_id, $cust_addr, $delivery_date;

        // Logo
        $this->Image('img/cherrypicks_logo.png',10,6,100);
        // Arial bold 15
        $this->SetFont('Arial','B',24);
        // Move to the right
        $this->Cell(140);
        // Title
        $this->Cell(0,0,'Deilvery Note',0,0,'C');
        // Line break
        $this->Ln(10);
        // Address
        $this->SetFont('Arial','',10);
        $this->Cell(20,10,'Cherrypicks',0,0,'L');
        $this->Cell(120);
        $this->Cell(20,10,'Order Date ',0,0,'R');
        $this->SetFont('Arial','',10);
        $this->Cell(20,10,$order_date,0,0,'L');
        $this->Ln(10);
        $this->SetFont('');
        $this->Cell(20,0,'18F, 10 Knutsford Terrace, Tsim Sha Tsui, Kowloon, Hong Kong',0,0,'L');
        $this->Cell(120);
        $this->Cell(20,0,'Order # ',0,0,'R');
        $this->SetFont('Arial','',10);
        $this->Cell(20,0,$order_no,0,0,'L');
        $this->Ln(5);
        $this->Cell(140);
        $this->SetFont('');
        $this->Cell(20,0,'Deilvery Note # ',0,0,'R');
        $this->SetFont('Arial','',10);
        $this->Cell(20,0,$dn_no,0,0,'L');
        $this->Ln(5);
        $this->Cell(140);
        $this->SetFont('');
        $this->Cell(20,0,'Customer ID ',0,0,'R');
        $this->SetFont('Arial','',10);
        $this->Cell(20,0,$cust_id,0,0,'L');
        $this->Ln(10);
        $this->Cell(140);
        $this->SetFont('');
        $this->Cell(20,0,'Delivery Date ',0,0,'R');
        $this->SetFont('Arial','',10);
        $this->Cell(20,0,$delivery_date,0,0,'L');

        $this->Ln(5);
    }

    // Page footer
    function Footer()
    {
        // Signature Section
        $this->SetY(-70);
        $this->SetFont('Arial','',10);
        $this->Cell(0,20,'Received by',0,0,'L');
        $this->Ln(30);
        $this->Cell(0,20,'__________________________________(Signature & Company Chop)',0,1,'L');
        $this->Cell(0,5,'Date:______________________________',0,1,'L');
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }

    // Table
    function FancyTable($header, $data)
    {
        // Colors, line width and bold font
        $this->SetFillColor(0,192,87);
        $this->SetTextColor(255);
        $this->SetDrawColor(0,0,0);
        $this->SetLineWidth(.3);
        $this->SetFont('Arial','B',10);
        // Header
        $w = array(20, 140, 30);
        for($i=0;$i<count($header);$i++)
            $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(224,235,255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Data
        $fill = false;
        $cnt = 1;
        foreach($data as $row)
            {
            $this->Cell($w[0],6,$cnt,'LR',0,'C',$fill);
            $this->Cell($w[1],6,$row['item_name'],'LR',0,'L',$fill);
            $this->Cell($w[2],6,$row['quantity'],'LR',0,'C',$fill);
            $this->Ln();
            $fill = !$fill;
            $cnt++;
            }
        // Closing line
        $this->Cell(array_sum($w),0,'','T');
        $this->Ln();

    }

    function ShowAppendix($serialnolist)
    {
        $this->AddPage('P','A4',0);
        $this->SetFont('Arial','B',24);
        //Appendix Title
        $this->Cell(50,5,'Appendix',0,1,'',false,'');
        $this->Ln(5);
        // Colors, line width and bold font
        $this->SetFont('Arial','',10);
        // Table Header
        $this->Cell(20,6,'Item #',1,0,'C',true);
        $this->Cell(60,6,'Item Name',1,0,'C',true);
        $this->Cell(60,6,'Serial No',1,0,'C',true);
        $this->Ln();

        $fill = false;
        $cnt = 1;
        foreach($serialnolist as $row)
            {
            $this->Cell(20,6,$cnt,'LR',0,'C',$fill);
            $this->Cell(60,6,$row['item_name'],'LR',0,'L',$fill);
            $this->Cell(60,6,$row['serial_no'],'LR',0,'C',$fill);
            $this->Ln();
            $fill = !$fill;
            $cnt++;
            }
        // Closing line
        $this->Cell(140,0,'','T');
        
    }

}
// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4',0);
//Shipping Address
$pdf->SetTextColor(255,255,255);
$pdf->SetFillColor(0,192,87);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(50,5,'Shipping Address',0,1,'',true,'');
$pdf->Ln(1);
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('');
$pdf->Cell(50,5,$cust_name,false,'L');
$pdf->Ln();
$pdf->MultiCell(100,5,$cust_addr,false,'L');
$pdf->Ln();
$pdf->Cell(50,5,'Attn: '.$cust_contact_person,false,'L');
$pdf->Ln();
$pdf->Cell(50,5,'Tel: '.$cust_tel,false,'L');
$pdf->Ln(5);

//Data Section
$header = array('Item #', 'Description', 'Quantity');
$pdf->SetFont('Arial','',10);
$pdf->FancyTable($header,$data);


//Show Appendix if there is serial number
if (count($serialnolist) > 0){
$pdf->Cell(20,6,'','L',0);
$pdf->Cell(0,6,'Please reference Appendix for serial numbers','R',1);
$pdf->Cell(190,0,'','T');
$pdf->ShowAppendix($serialnolist);
}
$pdf->Output('I',$dn_no.'.pdf');
