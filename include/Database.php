<?php
class Database
{

    /**
     * database connection object
     * @var \PDO
     */
    protected $pdo;

    /**
     * Connect to the database
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Return the pdo connection
     */
    public function getPdo()
    {
        return $this->pdo;
    }

    /**
     * Changes a camelCase table or field name to lowercase,
     * underscore spaced name
     *
     * @param  string $string camelCase string
     * @return string underscore_space string
     */
    protected function camelCaseToUnderscore($string)
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $string));
    }

    /**
     * Returns the ID of the last inserted row or sequence value
     *
     * @param  string $param Name of the sequence object from which the ID should be returned.
     * @return string representing the row ID of the last row that was inserted into the database.
     */
    public function lastInsertId($param = null)
    {
        return $this->pdo->lastInsertId($param);
    }

    /**
     * handler for dynamic CRUD methods
     *
     * Format for dynamic methods names -
     * Create:  insertTableName($arrData)
     * Retrieve: getTableNameByFieldName($value)
     * Update: updateTableNameByFieldName($value, $arrUpdate)
     * Delete: deleteTableNameByFieldName($value)
     *
     * @param  string     $function
     * @param  array      $arrParams
     * @return array|bool
     */
    public function __call($function, array $params = array())
    {
        if (!preg_match('/^(get|update|insert|delete)(.*)$/', $function, $matches)) {
            throw new \BadMethodCallException($function . ' is an invalid method Call');
        }

        if ('insert' == $matches[1]) {
            if (!is_array($params[0]) || count($params[0]) < 1) {
                throw new \InvalidArgumentException('insert values must be an array');
            }
            return $this->insert($this->camelCaseToUnderscore($matches[2]), $params[0]);
        }

        list($tableName, $fieldName) = explode('By', $matches[2], 2);
        if (!isset($tableName, $fieldName)) {
            throw new \BadMethodCallException($function . ' is an invalid method Call');
        }

        if ('update' == $matches[1]) {
            if (!is_array($params[1]) || count($params[1]) < 1) {
                throw new \InvalidArgumentException('update fields must be an array');
            }
            return $this->update(
                $this->camelCaseToUnderscore($tableName),
                $params[1],
                array($this->camelCaseToUnderscore($fieldName) => $params[0])
            );
        }

        //select and delete method
        return $this->{$matches[1]}(
            $this->camelCaseToUnderscore($tableName),
            array($this->camelCaseToUnderscore($fieldName) => $params[0])
        );
    }

    /**
     * Record retrieval method
     *
     * @param  string     $tableName name of the table
     * @param  array      $where     (key is field name)
     * @return array|bool (associative array for single records, multidim array for multiple records)
     */
    public function get($tableName,  $whereAnd  =   array(), $whereOr   =   array(), $whereLike =   array())
    {
        $cond   =   '';
        $s = 1;
        $params =   array();
        foreach ($whereAnd as $key => $val) {
            $cond   .=  " And " . $key . " = :a" . $s;
            $params['a' . $s] = $val;
            $s++;
        }
        foreach ($whereOr as $key => $val) {
            $cond   .=  " OR " . $key . " = :a" . $s;
            $params['a' . $s] = $val;
            $s++;
        }
        foreach ($whereLike as $key => $val) {
            $cond   .=  " OR " . $key . " like '% :a" . $s . "%'";
            $params['a' . $s] = $val;
            $s++;
        }
        $stmt = $this->pdo->prepare("SELECT  $tableName.* FROM $tableName WHERE 1 " . $cond);
        try {
            $stmt->execute($params);
            $res = $stmt->fetchAll();

            if (!$res || count($res) != 1) {
                return $res;
            }
            return $res;
        } catch (\PDOException $e) {
            throw new \RuntimeException("[" . $e->getCode() . "] : " . $e->getMessage());
        }
    }

    public function getAllRecords($tableName, $fields = '*', $cond = '', $orderBy = '', $limit = '')
    {
        //echo "SELECT  $tableName.$fields FROM $tableName WHERE 1 ".$cond." ".$orderBy." ".$limit;
        //print "<br>SELECT $fields FROM $tableName WHERE 1 ".$cond." ".$orderBy." ".$limit;
        $stmt = $this->pdo->prepare("SELECT $fields FROM $tableName WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        //print "SELECT $fields FROM $tableName WHERE 1 ".$cond." ".$orderBy." ";
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Vendor Records
    public function getVendorsRecords($tableName = 'vendors', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT a.id, a.vendor_name, a.vendor_addr, a.vendor_tel, a.vendor_email, a.vendor_contact_person, a.remarks, 
                                    CASE WHEN a.status = 0 THEN 'Inactive' ELSE 'Active' END AS status,
                                    b.user_name, b.id AS user_id 
                                    FROM $tableName a 
                                    JOIN users b on a.updated_by_user_id = b.id WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Colors Records
    public function getColorsRecords($tableName = 'colors', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT a.id, a.name, a.rgb,
                                    CASE WHEN a.status = 0 THEN 'Inactive' ELSE 'Active' END AS status,
                                    b.user_name, b.id AS user_id 
                                    FROM $tableName a 
                                    JOIN users b on a.updated_by_user_id = b.id WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Enclosures Records
    public function getEnclosuresRecords($tableName = 'enclosures', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT a.id, a.enclosure_type, a.remarks,
                                    CASE WHEN a.status = 0 THEN 'Inactive' ELSE 'Active' END AS status,
                                    b.user_name, b.id AS user_id 
                                    FROM $tableName a 
                                    JOIN users b on a.updated_by_user_id = b.id WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Firmwares Records
    public function getFirmwaresRecords($tableName = 'firmwares', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT a.id, a.version, a.remarks,
                                    CASE WHEN a.status = 0 THEN 'Inactive' ELSE 'Active' END AS status,
                                    b.user_name, b.id AS user_id 
                                    FROM $tableName a 
                                    JOIN users b on a.updated_by_user_id = b.id WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Item Types Records
    public function getItemTypesRecords($tableName = 'item_types', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT a.id, a.name,                                     
                                    CASE WHEN a.serial_enable = 0 THEN 'False' ELSE 'True' END AS serial_enable,
                                    CASE WHEN a.status = 0 THEN 'Inactive' ELSE 'Active' END AS status,
                                    b.user_name, b.id AS user_id 
                                    FROM $tableName a 
                                    JOIN users b on a.updated_by_user_id = b.id WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Locations Records
    public function getLocationsRecords($tableName = 'locations', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT a.id, a.location_code, a.location_desc,
                                        CASE WHEN a.status = 0 THEN 'Inactive' ELSE 'Active' END AS status,
                                        b.user_name, b.id AS user_id 
                                        FROM $tableName a 
                                        JOIN users b on a.updated_by_user_id = b.id WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Customers Records
    public function getCustomersRecords($tableName = 'customers', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT a.id, a.cust_name, a.cust_addr, a.cust_tel, a.cust_email, cust_contact_person, a.remarks,
                                        CASE WHEN a.status = 0 THEN 'Inactive' ELSE 'Active' END AS status,
                                        b.user_name, b.id AS user_id 
                                        FROM $tableName a 
                                        JOIN users b on a.updated_by_user_id = b.id WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Projects Records
    public function getProjectsRecords($tableName = 'projects', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT a.id, a.project_code, a.project_name, a.project_owner_team, a.project_pic, a.remarks,
                                        CASE WHEN a.status = 0 THEN 'Inactive' ELSE 'Active' END AS status,
                                        b.user_name, b.id AS user_id 
                                        FROM $tableName a 
                                        JOIN users b on a.updated_by_user_id = b.id WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Items Records
    public function getItemsRecords($tableName = 'items', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT i.id as iid, v.vendor_name as vname, i.item_code as icode, i.item_name as iname, it.name as itname, 
                                    i.model_no as imodelno, i.remarks as iremarks, c.name as cname, 
                                    e.enclosure_type as etype, f.version as fversion, 
                                        CASE WHEN i.status = 0 THEN 'Inactive' ELSE 'Active' END AS istatus,
                                        b.user_name, i.id AS user_id 
                                        FROM $tableName i
                                        LEFT JOIN enclosures e on i.enclosure_id = e.id
                                        LEFT JOIN firmwares f on i.firmware_id = f.id 
                                        JOIN users b on i.updated_by_user_id = b.id
                                        JOIN vendors v on i.vendor_id = v.id
                                        JOIN item_types it on i.item_type_id = it.id
                                        JOIN colors c on i.color_id = c.id
                                        WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Receipts
    public function getReceiptsRecords($tableName = 'receipts', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT a.id, a.receipt_no, a.receipt_date, v.vendor_name as vname, a.remarks, 
                                            CASE WHEN a.status = 0 THEN 'Inactive' ELSE 'Active' END AS status,
                                            b.user_name, b.id AS user_id 
                                            FROM $tableName a 
                                            JOIN vendors v on a.vendor_id = v.id
                                            JOIN users b on a.updated_by_user_id = b.id WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Receipt Details
    public function getReceiptDetails($tableName = 'receipt_details', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT a.id, i.item_name, a.unit_price, a.qty,
                                            CASE WHEN a.status = 0 THEN 'Inactive' ELSE 'Active' END AS status,
                                            b.user_name, b.id AS user_id 
                                            FROM $tableName a 
                                            JOIN items i on a.item_id = i.id
                                            JOIN users b on a.updated_by_user_id = b.id WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Orders
    public function getOrdersRecords($tableName = 'orders', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT a.id, a.order_no, a.order_date, c.cust_name as cname, p.project_name, a.remarks,
                                                CASE WHEN a.status = 0 THEN 'Inactive' ELSE 'Active' END AS status,
                                                b.user_name, b.id AS user_id 
                                                FROM $tableName a 
                                                JOIN customers c on c.id = a.customer_id
                                                JOIN projects p on a.project_id = p.id
                                                JOIN users b on a.updated_by_user_id = b.id WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Order Details
    public function getOrderDetails($tableName = 'order_details', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT a.id, i.item_name, a.unit_price, a.qty,
                                            CASE WHEN a.status = 0 THEN 'Inactive' ELSE 'Active' END AS status,
                                            b.user_name, b.id AS user_id 
                                            FROM $tableName a 
                                            JOIN items i on a.item_id = i.id
                                            JOIN users b on a.updated_by_user_id = b.id WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get In Bound Records
    public function getInBoundRecords($tableName = 'inbounds', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT a.id, r.receipt_no, a.due_date, a.remarks,
                                                    CASE WHEN a.status = 0 THEN 'Inactive' ELSE 'Active' END AS status,
                                                    b.user_name, b.id AS user_id 
                                                    FROM $tableName a 
                                                    JOIN receipts r on r.id = a.receipt_id
                                                    JOIN users b on a.updated_by_user_id = b.id WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Inbound Details
    public function getInBoundDetails($tableName = 'inbound_details', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT a.id, i.item_name, a.box_no, a.serial_no, a.case_no, a.qty, l.location_code,
                                                CASE WHEN a.status = 0 THEN 'Inactive' ELSE 'Active' END AS status,
                                                b.user_name, b.id AS user_id 
                                                FROM $tableName a 
                                                JOIN items i on a.item_id = i.id
                                                JOIN locations l on a.location_id = l.id
                                                JOIN users b on a.updated_by_user_id = b.id WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Out Bound Records
    public function getOutBoundRecords($tableName = 'outbounds', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT a.id, o.order_no, a.record_date, a.remarks, a.delivery_date,
                                                    CASE WHEN a.status = 0 THEN 'Inactive' ELSE 'Active' END AS status,
                                                    b.user_name, b.id AS user_id 
                                                    FROM $tableName a 
                                                    JOIN orders o on o.id = a.order_id
                                                    JOIN users b on a.updated_by_user_id = b.id WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Outbound Details
    public function getOutBoundDetails($tableName = 'outbound_details', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT a.id, i.item_name, a.box_no, a.serial_no, a.case_no, a.qty, l.location_code,
                                                CASE WHEN a.status = 0 THEN 'Inactive' ELSE 'Active' END AS status,
                                                b.user_name, b.id AS user_id 
                                                FROM $tableName a 
                                                JOIN items i on a.item_id = i.id
                                                JOIN locations l on a.location_id = l.id
                                                JOIN users b on a.updated_by_user_id = b.id WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get Users Records
    public function getUsersRecords($tableName = 'users', $cond = '', $orderBy = '', $limit = '')
    {
        $stmt = $this->pdo->prepare("SELECT id, user_name, user_role_id FROM $tableName WHERE 1 " . $cond . " " . $orderBy . " " . $limit);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    // Get AES Key
    public function getAESKey()
    {
        $stmt = $this->pdo->prepare("SELECT misc_value from misc where misc_option = 'aes_key'");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function getRecFrmQry($query)
    {
        //echo $query;
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getRecFrmQryStr($query)
    {
        //echo $query;
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        return array();
    }
    public function getQueryCount($tableName, $field, $cond = '')
    {
        $stmt = $this->pdo->prepare("SELECT count($field) as total FROM $tableName WHERE 1 " . $cond);
        try {
            $stmt->execute();
            $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if (!$res || count($res) != 1) {
                return $res;
            }
            return $res;
        } catch (\PDOException $e) {
            throw new \RuntimeException("[" . $e->getCode() . "] : " . $e->getMessage());
        }
    }

    /**
     * Update Method
     *
     * @param  string $tableName
     * @param  array  $set       (associative where key is field name)
     * @param  array  $where     (associative where key is field name)
     * @return int    number of affected rows
     */
    public function update($tableName, array $set, array $where)
    {
        $arrSet = array_map(
            function ($value) {
                return $value . '=:' . $value;
            },
            array_keys($set)
        );

        $stmt = $this->pdo->prepare(
            "UPDATE $tableName SET " . implode(',', $arrSet) . ' WHERE ' . key($where) . '=:' . key($where) . 'Field'
        );

        foreach ($set as $field => $value) {
            $stmt->bindValue(':' . $field, $value);
        }
        $stmt->bindValue(':' . key($where) . 'Field', current($where));
        try {
            $stmt->execute();

            return $stmt->rowCount();
        } catch (\PDOException $e) {
            throw new \RuntimeException("[" . $e->getCode() . "] : " . $e->getMessage());
        }
    }

    public function updateUsers($user_id, $password, $user_role_id)
    {
        $stmt = $this->pdo->prepare(
            "UPDATE users SET user_password = md5('".$password."'), user_role_id = ".$user_role_id." where id =".$user_id
        );
        try {
            $stmt->execute();

            return $stmt->rowCount();
        } catch (\PDOException $e) {
            throw new \RuntimeException("[" . $e->getCode() . "] : " . $e->getMessage());
        }
    }

    /**
     * Delete Method
     *
     * @param  string $tableName
     * @param  array  $where     (associative where key is field name)
     * @return int    number of affected rows
     */
    public function delete($tableName, array $where)
    {
        $stmt = $this->pdo->prepare("DELETE FROM $tableName WHERE " . key($where) . ' = ?');
        try {
            $stmt->execute(array(current($where)));

            return $stmt->rowCount();
        } catch (\PDOException $e) {
            throw new \RuntimeException("[" . $e->getCode() . "] : " . $e->getMessage());
        }
    }
    // *** New Delete Function ***
    public function deactivate($tableName, array $where)
    {
        $stmt = $this->pdo->prepare("UPDATE $tableName set status = 0 WHERE " . key($where) . ' = ?');
        try {
            $stmt->execute(array(current($where)));

            return $stmt->rowCount();
        } catch (\PDOException $e) {
            throw new \RuntimeException("[" . $e->getCode() . "] : " . $e->getMessage());
        }
    }

    public function activate($tableName, array $where)
    {
        $stmt = $this->pdo->prepare("UPDATE $tableName set status = 1 WHERE " . key($where) . ' = ?');
        try {
            $stmt->execute(array(current($where)));

            return $stmt->rowCount();
        } catch (\PDOException $e) {
            throw new \RuntimeException("[" . $e->getCode() . "] : " . $e->getMessage());
        }
    }

    public function deleteQry($query)
    {
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
    }


    /**
     * Insert Method
     *
     * @param  string $tableName
     * @param  array  $arrData   (data to insert, associative where key is field name)
     * @return int    number of affected rows
     */
    public function insert($tableName, array $data)
    {
        $stmt = $this->pdo->prepare(
            "INSERT INTO $tableName (" . implode(',', array_keys($data)) . ")
            VALUES (" . implode(',', array_fill(0, count($data), '?')) . ")"
        );
        try {
            $stmt->execute(array_values($data));
            return $stmt->rowCount();
        } catch (\PDOException $e) {
            throw new \RuntimeException("[" . $e->getCode() . "] : " . $e->getMessage());
        }
    }
    public function insertUsers($user_name, $password, $user_role_id)
    {
        $stmt = $this->pdo->prepare(
            "INSERT INTO users (user_name, user_password, user_role_id)
            VALUES ('".$user_name."', md5('".$password."'),".$user_role_id.")"
        );
        try {
            $stmt->execute();
            return $stmt->rowCount();
        } catch (\PDOException $e) {
            throw new \RuntimeException("[" . $e->getCode() . "] : " . $e->getMessage());
        }
    }

    /**
     * Print array Method
     *
     * @param  array 
     */
    public function arprint($array)
    {
        print "<pre>";
        print_r($array);
        print "</pre>";
    }
    /**
     * Maker Model Name Method
     *
     * @param  Int make id
     * @param  Int name id 
     */
    public function getModelMake($makeID, $nameID)
    {
        $vehMakeData    =   self::getRecFrmQry('SELECT veh_make_id,veh_make_name FROM tb_vehicle_make WHERE veh_make_id="' . $makeID . '"');
        $vehNameData    =   self::getRecFrmQry('SELECT veh_name_id,veh_name FROM tb_vehicle_name WHERE veh_name_id="' . $nameID . '"');
        return $vehMakeData[0]['veh_make_name'] . ' ' . $vehNameData[0]['veh_name'];
    }
    /**
     * Cache Method
     *
     * @param  string QUERY
     * @param  Int Time default 0 set 
     */
    public function getCache($sql, $cache_min = 0)
    {
        $f = 'cache/' . md5($sql);
        if ($cache_min != 0 and file_exists($f) and ((time() - filemtime($f)) / 60 < $cache_min)) {
            $arr = unserialize(file_get_contents($f));
        } else {
            unlink($f);
            $arr = self::getRecFrmQry($sql);
            if ($cache_min != 0) {
                $fp = fopen($f, 'w');
                fwrite($fp, serialize($arr));
                fclose($fp);
            }
        }
        return $arr;
    }

    public function getUsername($uid)
    {
        $stmt = $this->pdo->prepare("SELECT user_name FROM users where id = $uid");
        $stmt->execute();
        $unr = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $unr;
    }

    public function getAllUsernames()
    {
        $stmt = $this->pdo->prepare("SELECT id, user_name FROM users");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getUserRolesByUserID($user_id)
    {
        $stmt = $this->pdo->prepare("SELECT user_role_id FROM users where id = ".$user_id);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getAllUserRoles()
    {
        $stmt = $this->pdo->prepare("SELECT distinct(user_role_id) FROM users");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }


    public function getAllColors()
    {
        $stmt = $this->pdo->prepare("SELECT id as cid, name as cname FROM colors  where status > 0");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getAllEnclosureTypes()
    {
        $stmt = $this->pdo->prepare("SELECT id as eid, enclosure_type as etype FROM enclosures  where status > 0");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getAllFirmwares()
    {
        $stmt = $this->pdo->prepare("SELECT id as fid, version as fversion FROM firmwares  where status > 0");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getAllVendors()
    {
        $stmt = $this->pdo->prepare("SELECT id as vid, vendor_name as vname FROM vendors where status > 0");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getAllItemTypes()
    {
        $stmt = $this->pdo->prepare("SELECT id as itid, name as itname FROM item_types where status > 0");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getAllItemNames()
    {
        $stmt = $this->pdo->prepare("SELECT id , item_name FROM items where status > 0");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getAllReceipts()
    {
        $stmt = $this->pdo->prepare("SELECT id , receipt_no FROM receipts where status > 0");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getReceiptNo($rid)
    {
        $stmt = $this->pdo->prepare("SELECT receipt_no FROM receipts where id = $rid limit 1");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function getAllOrders()
    {
        $stmt = $this->pdo->prepare("SELECT id , order_no FROM orders where status > 0");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getOrderNo($rid)
    {
        $stmt = $this->pdo->prepare("SELECT order_no FROM orders where id = $rid limit 1");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function getAllProjects()
    {
        $stmt = $this->pdo->prepare("SELECT id, project_name FROM projects where status > 0");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getAllCustomers()
    {
        $stmt = $this->pdo->prepare("SELECT id, cust_name FROM customers where status > 0");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getAllLocations()
    {
        $stmt = $this->pdo->prepare("SELECT id, location_code FROM locations where status > 0");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }


    //For DN Section
    public function getDNItems($outbound_id)
    {
        $stmt = $this->pdo->prepare("select CONCAT(i.item_code,' - ',i.item_name) as item_name, count(od.id) as quantity from outbound_details od
        join outbounds o
        on o.id = od.outbound_id
        join items i
        on i.id = od.item_id
        where o.id = ".$outbound_id."
        group by 1");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getDNDetails($outbound_id)
    {
        $stmt = $this->pdo->prepare("select o.order_date, o.order_no, CONCAT('CUST',o.customer_id) as `cust_id`, c.cust_addr, c.cust_name, c.cust_contact_person, c.cust_tel, ob.delivery_date from outbounds ob
        join orders o
        on o.id = ob.order_id
        join customers c
        on c.id = o.customer_id
        where ob.id = ".$outbound_id);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getDNSerialNo($outbound_id)
    {
        $stmt = $this->pdo->prepare("select i.item_name, od.serial_no from outbound_details od
        join items i
        on i.id = od.item_id
        join item_types it
        on it.id = i.item_type_id
        where it.serial_enable = 1 and od.outbound_id = ".$outbound_id);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function CheckDeliveryDate($outbound_id)
    {
        $stmt = $this->pdo->prepare("select delivery_date from outbounds where id = '" .$outbound_id."'");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }
    public function getRecieptItemNames($receipt_id)
    {
        $stmt = $this->pdo->prepare("select id, item_name from items where id in (select item_id from receipt_details where receipt_id = '".$receipt_id."')");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getRecieptItemNamesByInboundID($inbound_id)
    {
        $stmt = $this->pdo->prepare("select id, item_name from items where id in (select item_id from receipt_details where receipt_id = (select receipt_id from inbounds where id = ".$inbound_id."))");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getAllInboundItemNames()
    {
        $stmt = $this->pdo->prepare("select id, item_name from items where id in (select item_id from inbound_details)");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getAllInboundItemSN()
    {
        $stmt = $this->pdo->prepare("select ind.id, i.item_name, ind.serial_no from inbound_details ind
        join items i
        on ind.item_id = i.id");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getInboundDetailId($item_id, $serial_no)
    {
        $stmt = $this->pdo->prepare("select id from inbound_details where item_id = ". $item_id . " and serial_no = '" . $serial_no. "'");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getOldestInboundDetailIdByItemID($item_id)
    {
        $stmt = $this->pdo->prepare("select id from inbound_details where item_id = ".$item_id. " LIMIT 1");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function getItemSerialEnable()
    {
        $stmt = $this->pdo->prepare("select i.id, it.serial_enable from items i
        join item_types it
        on i.item_type_id = it.id");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getItemNameByID($item_id)
    {
        $stmt = $this->pdo->prepare("select item_name from items where id = ".$item_id);
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function getInStockSummary()
    {
        $stmt = $this->pdo->prepare("select it.item_name, sum(ind.qty) as `Qty` from (select id, inbound_detail_id, max(updated_at) as MaxUpdated from item_histories
        group by inbound_detail_id) t2
        join item_histories t1
        on t2.inbound_detail_id = t1.inbound_detail_id
        and t2.MaxUpdated = t1.updated_at
        join inbound_details ind
        on ind.id = t1.inbound_detail_id
        join items it
        on ind.item_id = it.id
        where t1.status in ('Inbound', 'Cancel')
        group by it.item_name
        union
        select a.item_name, coalesce(sum(Qty),0) as Qty from (
            select it.item_name, sum(ind.Qty) as `Qty` from inbound_details ind
            join items it
			on ind.item_id = it.id
			join item_types ity
			on ity.id = it.item_type_id
			where ity.serial_enable < 1
			group by ind.item_id
            UNION
            select it.item_name, sum(Qty) as Qty from outbound_details outd
            join items it
			on outd.item_id = it.id
			join item_types ity
			on ity.id = it.item_type_id
			where ity.serial_enable < 1
			group by outd.item_id)a ;");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getLatestOutboundDetailsSerialNo()
    {
        $stmt = $this->pdo->prepare("select serial_no from outbound_details order by id desc LIMIT 1");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getItemID($item_name)
    {
        $stmt = $this->pdo->prepare("select id from items where item_name = '" .$item_name."' LIMIT 1");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function getLocationID($location_code)
    {
        $stmt = $this->pdo->prepare("select id from locations where location_code = '" .$location_code."' LIMIT 1");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function getLatestInboundDetailID()
    {
        $stmt = $this->pdo->prepare("select max(id) from inbound_details");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function getInboundDetailIDBySN($serial_no)
    {
        $stmt = $this->pdo->prepare("select id from inbound_details where serial_no = '".$serial_no."'");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function CheckSerialEnable($item_id)
    {
        $stmt = $this->pdo->prepare("select serial_enable from item_types where id = (select item_type_id from items where id ='" .$item_id."')");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function TotalInboundDetailQty($inbound_id)
    {
        $stmt = $this->pdo->prepare("select sum(qty) from inbound_details where inbound_id = '" .$inbound_id."' and item_id > 0");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function CheckUserExist($user_name)
    {
        $stmt = $this->pdo->prepare("select count(id) from users where user_name = '" .$user_name."'");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function CheckSNExist($table_name, $serial_no)
    {
        $stmt = $this->pdo->prepare("select serial_no from ".$table_name. " where serial_no = '" .$serial_no."'");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function CheckLatestStatus($serial_no)
    {
        $stmt = $this->pdo->prepare("select status from item_histories where id = (
            select max(id) from item_histories where inbound_detail_id in (
                select id from inbound_details where serial_no ='" .$serial_no."'))");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function CheckSerialEnableByItemName($item_name)
    {
        $stmt = $this->pdo->prepare("select serial_enable from item_types where id = 
        (select item_type_id from items where item_name = '".$item_name."')");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function GetCurrentVersion()
    {
        $stmt = $this->pdo->prepare("select misc_value from misc where misc_option = 'version'");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function GetUserID($user_name)
    {
        $stmt = $this->pdo->prepare("select id from users where user_name = '".$user_name."'");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function GetUserPassword($user_name)
    {
        $stmt = $this->pdo->prepare("select user_password from users where user_name = '".$user_name."'");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    public function HashPassword($password)
    {
        $stmt = $this->pdo->prepare("select md5('".$password."') from users LIMIT 1");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }

    //Reports Data
    public function getAllInStockItems()
    {
        $stmt = $this->pdo->prepare("select i.item_name as `Item Name`, 
        v.vendor_name as `Vendor Name`, 
        it.name as `Item Type`, 
        c.name as `Color`, 
        en.enclosure_type as `Enclosure Type`, 
        ind.serial_no as `Serial Number`, 
        ind.qty as `Qty`,
        loc.location_code as `Location Code`,
        ind.box_no as `Box No`, 
        now() + interval 8 hour as `Record Generation Time`
        from item_histories ith
        join inbound_details ind
        on ind.id = ith.inbound_detail_id
        join items i
        on ind.item_id = i.id
        join vendors v
        on v.id = i.vendor_id
        join colors c
        on c.id = i.color_id
        left outer join enclosures en
        on en.id = i.enclosure_id
        join item_types it
        on it.id = i.item_type_id
        join locations loc
        on loc.id = ind.location_id
        join inbounds inb
        on inb.id = ind.inbound_id
        where ith.status in ('Inbound', 'Outbound Cancel')
        group by ith.inbound_detail_id");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getAllInboundItems()
    {
        $stmt = $this->pdo->prepare("select it.name as `Item Type`, c.name as `Color`, en.enclosure_type as `Enclosure Type`, 
        ind.serial_no as `Serial Number`, ind.qty as `Qty`, v.vendor_name as `Vendor Name`, ind.box_no as `Box No`, 
        loc.location_code as `Location Code`, rec.receipt_no as `Receipt No`, inb.due_date as `Inbound Date`, 
        ith.updated_at as `Record Time`, usr.user_name as `Updated By`
        from item_histories ith
        join inbound_details ind
        on ind.id = ith.inbound_detail_id
        join items i
        on ind.item_id = i.id
        join vendors v
        on v.id = i.vendor_id
        join colors c
        on c.id = i.color_id
        left outer join enclosures en
        on en.id = i.enclosure_id
        join item_types it
        on it.id = i.item_type_id
        join locations loc
        on loc.id = ind.location_id
        join inbounds inb
        on inb.id = ind.inbound_id
        join receipts rec
        on rec.id = inb.receipt_id
        join users usr
        on usr.id = ith.updated_by_user_id
        where ith.status in ('Inbound', 'Outbound Cancel')
        group by ith.inbound_detail_id");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getAllOutboundItems()
    {
        $stmt = $this->pdo->prepare("select ords.order_no as `Order No`, ords.order_date as `Order Date`, it.name as `Item Type`, 
        c.name as `Color`, en.enclosure_type as `Enclosure Type`, 
        outd.serial_no as `Serial Number`,  ind.qty as `Qty`, outd.box_no as `Box No`, loc.location_code as `Location Code`, v.vendor_name as `Vendor Name`,
        max(ith.status) as `Remarks`, outb.delivery_date as `DeliveryDate`, outb.id as `Record No`,
        ith.updated_at as `Record Time`, usr.user_name as `Updated By`
        from item_histories ith
        join outbound_details outd
        on outd.inbound_detail_id = ith.inbound_detail_id
        join outbounds outb
        on outb.id = outd.outbound_id
        join inbound_details ind
        on ind.id = outd.inbound_detail_id
        join items i
        on i.id = ind.item_id
        join colors c
        on c.id = i.color_id
        left outer join enclosures en
        on en.id = i.enclosure_id
        join vendors v
        on v.id = i.vendor_id
        join item_types it
        on it.id = i.item_type_id
        join locations loc
        on loc.id = outd.location_id
        join orders ords
        on ords.id = outb.order_id
        join users usr
        on usr.id = ith.updated_by_user_id
        where ith.status = 'Outbound'
        group by ith.inbound_detail_id");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getFullTransaction()
    {
        $stmt = $this->pdo->prepare("select it.name as `Item Type`, c.name as `Color`, en.enclosure_type as `Enclosure Type`, 
        ind.serial_no as `Serial Number`, v.vendor_name as `Vendor Name`, ind.box_no as `Box No`, ind.qty as `Inbound Qty`,
        ith.status as `Action`, loc.location_code as `Location Code`, rec.receipt_no as `Receipt No`, 
        inb.due_date as `Inbound Date`, ords.order_no as `Order No`, outd.qty as `Outbound Qty`,
        ords.order_date as `Order Date`, ith.updated_at as `Updated at`, usr.user_name as `Updated By`
        from item_histories ith
        join inbound_details ind
        on ind.id = ith.inbound_detail_id
        join items i
        on ind.item_id = i.id
        join vendors v
        on v.id = i.vendor_id
        join colors c
        on c.id = i.color_id
        left outer join enclosures en
        on en.id = i.enclosure_id
        join item_types it
        on it.id = i.item_type_id
        join locations loc
        on loc.id = ind.location_id
        join inbounds inb
        on inb.id = ind.inbound_id
        join receipts rec
        on rec.id = inb.receipt_id
        left outer join outbound_details outd
        on outd.inbound_detail_id = ind.id
        left outer join outbounds outb
        on outb.id = outd.outbound_id
        left outer join orders ords
        on outb.order_id = ords.id
        join users usr
        on usr.id = ith.updated_by_user_id
        order by ith.inbound_detail_id");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function CheckRemaining($item_id)
    {
        $stmt = $this->pdo->prepare("select coalesce(sum(Qty),0) as Remaining from (
            select sum(Qty) as Qty from inbound_details ind
            where ind.item_id = '".$item_id."'
            UNION
            select sum(Qty) as Qty from outbound_details outd
            where outd.item_id = '".$item_id."') a");
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        return $rows;
    }
}
