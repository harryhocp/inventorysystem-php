<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
	header("location: login.php");
	exit;
}
?>
<?php include_once('config.php');
$table = "colors";

if (isset($_REQUEST['editId']) and $_REQUEST['editId'] != "") {
	$row	=	$db->getAllRecords($table, '*', ' AND id="' . $_REQUEST['editId'] . '"');
}


if (isset($_REQUEST['submit']) and $_REQUEST['submit'] != "") {
	extract($_REQUEST);
	$data	=	array(
		'name' => $name,
		'rgb' => $rgb,
		'updated_by_user_id' => $_SESSION["id"]
	);
	$update	=	$db->update($table, $data, array('id' => $editId));
	if ($update) {
		header('location: browse-'.$table.'.php?msg=rus');
		exit;
	} else {
		header('location: browse-'.$table.'.php?msg=rnu');
		exit;
	}
}
?>
<!doctype html>
<html lang="en-US" xmlns:fb="https://www.facebook.com/2008/fbml" xmlns:addthis="https://www.addthis.com/help/api-spec" prefix="og: http://ogp.me/ns#" class="no-js">

<head>

	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>starbeacon - Inventory System</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<script src="https://use.fontawesome.com/4102c26c2b.js"></script>

	<!-- Custom fonts for this template-->
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body>


	<!-- Page Wrapper -->
	<div id="wrapper">

		<!-- Sidebar -->
		<ul class="navbar-nav bg-primary sidebar sidebar-dark accordion" id="accordionSidebar">

			<!-- Sidebar - Brand -->
			<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
				<img src="img/starbeacon_logo.png" width="100%" height="46" alt="" />
			</a>

			<!-- Title -->
			<li class="text-white text-center text-lg" style="padding-top: 10px; padding-bottom: 10px;" data-toggle="#" data-target="#" aria-expanded="true" aria-controls="#">
				<span>Inventory System</span>
			</li>
			<!-- Divider -->
			<hr class="sidebar-divider my-0">


			<li class="text-white text-left" data-toggle="#" data-target="#" aria-expanded="true" aria-controls="#">
				<i class="fa fa-user" style="margin-left: 20px; margin-right: 10px;padding-top: 18px; padding-bottom: 18px;"></i>
				<?php echo htmlspecialchars($_SESSION["username"]); ?>

			</li>


			<!-- Divider -->
			<hr class="sidebar-divider my-0">

			<!-- Master Records Drop Down-->
			<li class="nav-item active">
				<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseMD" aria-expanded="true" aria-controls="collapseMD">
					<i class="fas fa-fw fa-cog"></i>
					<span>Master Records</span>
				</a>
				<div id="collapseMD" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
					<div class="bg-white collapse-inner rounded">
						<h6 class="collapse-header">Main:</h6>
						<a class="collapse-item" href="browse-items.php"><b>Items</a>
						<a class="collapse-item" href="browse-locations.php">Locations</a>
						<a class="collapse-item" href="browse-customers.php">Customers</a>
<a class="collapse-item" href="browse-projects.php">Projects</a>
						<a class="collapse-item" href="browse-users.php">Users</a></b>						<h6 class="collapse-header">Items:</h6>
						<a class="collapse-item" href="browse-vendors.php">Vendors</a>
						<a class="collapse-item active" href="browse-colors.php">Colors</a>
						<a class="collapse-item" href="browse-enclosures.php">Enclosures</a>
						<a class="collapse-item" href="browse-firmwares.php">Firmwares</a>
						<a class="collapse-item" href="browse-item_types.php">Item Types</a>
					</div>
				</div>
			</li>

			<!-- Divider -->
			<hr class="sidebar-divider my-0">
			<!-- In-Out Records Drop Down-->
			<li class="nav-item">
				<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseIOD" aria-expanded="true" aria-controls="collapseIOD">
					<i class="fas fa-fw fa-cog"></i>
					<span>In Out Records</span>
				</a>
				<div id="collapseIOD" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
					<div class="bg-white collapse-inner rounded">
						<a class="collapse-item" href="browse-receipts.php">Receipts</a>
						<a class="collapse-item" href="browse-orders.php">Orders</a>
						<a class="collapse-item" href="browse-inbounds.php">Inbounds</a>
						<a class="collapse-item" href="browse-outbounds.php">Outbounds</a>
						<!-- <a class="collapse-item" href="browse-relocations.php">Relocations</a> -->
					</div>
				</div>
			</li>
			<!-- Divider -->
			<hr class="sidebar-divider my-0">



			<!-- Show Logging In User & Sign Out Button-->
			<li class="text-white" style="padding-top: 10px; padding-bottom: 5px; margin-left: 5px;" data-toggle="#" data-target="#" aria-expanded="true" aria-controls="#">

				<button href="javascript://" onclick="self.parent.location='logout.php'" button type="button" class="btn btn-primary">
					<span class="icon text-white-50">
						<i class="fa fa-sign-out" aria-hidden="true"></i>
					</span>
					<span class="text" style="margin-left: 5px">Sign Out</span>
				</button>
			</li>
			<!-- Version -->
			<li>
				<div class="container text-gray-300 text-sm-center font-weight-lighter">
					<small>v <?php echo $db->GetCurrentVersion() ?></small>
				</div>
			</li>
		</ul>
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<div class="bg-light border-bottom shadow-sm sticky-top">
					<div class="container">
						<header>
						</header>
					</div>
					<!--/.container-->
				</div>

				<div class="container-fluid">
					<h1 class="h3 mb-0 text-gray-800" style="padding-top: 20px;padding-bottom: 10px;"><?php echo ucfirst($table) ?></h1>

					<?php
					if (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "un") {
						echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> '.ucfirst(substr($table, 0, -1)).' name is mandatory field!</div>';
					} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "ras") {
						echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Record added successfully!</div>';
					} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "rna") {
						echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Record not added <strong>Please try again!</strong></div>';
					} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "dsd") {
						echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Please delete a '.substr($table, 0, -1).' and then try again <strong>We set limit for security reasons!</strong></div>';
					}
					?>

					<div class="card">
						<div class="card-header"><i class="fa fa-fw fa-plus-circle"></i> <strong>Edit <?php echo ucfirst(substr($table, 0, -1)) ?> - id: <?php echo $row[0]['id']; ?></strong> <a href="browse-<?php echo $table ?>.php" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-globe"></i> Browse <?php echo ucfirst($table) ?></a></div>
						<div class="card-body">

							<div class="col-sm-6">
								<h5 class="card-title">Fields with <span class="text-danger">*</span> are mandatory!</h5>
								<form method="post">

									<div class="form-group">
										<label>Color Name <span class="text-danger">*</span></label>
										<input type="text" name="name" id="name" class="form-control" value="<?php echo $row[0]['name']; ?>" required>
									</div>
									<div class="form-group">
										<label>RGB</label>
										<input type="text" name="rgb" id="rgb" class="form-control" value="<?php echo $row[0]['rgb']; ?>">
									</div>


									<div class="form-group">
										<input type="hidden" name="editId" id="editId" value="<?php echo $_REQUEST['editId'] ?>">
										<button type="submit" name="submit" value="submit" id="submit" class="btn btn-primary"><i class="fa fa-fw fa-edit"></i> Update <?php echo ucfirst(substr($table, 0, -1)) ?></button>
									</div>

								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End of Main Content -->

			<!-- Footer -->
			<footer class="sticky-footer bg-white">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Copyright &copy; starbeacon 2019</span>
					</div>
				</div>
			</footer>
			<!-- End of Footer -->
		</div>
	</div>


	<!-- End of Content Wrapper -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

</body>

</html>