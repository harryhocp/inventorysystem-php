<?php
// Initialize the session
session_start();
include_once('config.php');
// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
  header("location: login.php");
  exit;
}
?>

<!DOCTYPE html>
<html lang="en">



<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>starbeacon - Inventory System</title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <script src="https://use.fontawesome.com/4102c26c2b.js"></script>

  <!-- Custom fonts for this template-->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <style>
    .signoutbtn {
      background-color: darkblue;
      color: white;
      border: 1px solid white;
      transition-duration: 0.4s;
    }

    .signoutbtn:hover {
      background-color: orangered;
      color: white;
    }
  </style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <img src="img/starbeacon_logo.png" width="100%" height="46" alt="" />
      </a>

      <!-- Title -->
      <li class="text-white text-center text-lg" style="padding-top: 10px; padding-bottom: 10px;" data-toggle="#" data-target="#" aria-expanded="true" aria-controls="#">
        <span>Inventory System</span>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">


      <li class="text-white text-left" data-toggle="#" data-target="#" aria-expanded="true" aria-controls="#">
        <i class="fa fa-user" style="margin-left: 20px; margin-right: 10px;padding-top: 18px; padding-bottom: 18px;"></i>
        <?php echo htmlspecialchars($_SESSION["username"]); ?>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Master Records Drop Down-->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseMD" aria-expanded="true" aria-controls="collapseMD">
          <i class="fas fa-fw fa-cog"></i>
          <span>Master Records</span>
        </a>
        <div id="collapseMD" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white collapse-inner rounded">
            <h6 class="collapse-header">Main:</h6>
            <a class="collapse-item" href="browse-items.php"><b>Items</a>
            <a class="collapse-item" href="browse-locations.php">Locations</a>
            <a class="collapse-item" href="browse-customers.php">Customers</a>
            <a class="collapse-item" href="browse-projects.php">Projects</a>
            <a class="collapse-item" href="browse-users.php">Users</a></b>
            <h6 class="collapse-header">Items:</h6>
            <a class="collapse-item" href="browse-vendors.php">Vendors</a>
            <a class="collapse-item" href="browse-colors.php">Colors</a>
            <a class="collapse-item" href="browse-enclosures.php">Enclosures</a>
            <a class="collapse-item" href="browse-firmwares.php">Firmwares</a>
            <a class="collapse-item" href="browse-item_types.php">Item Types</a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <!-- In-Out Records Drop Down-->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseIOD" aria-expanded="true" aria-controls="collapseIOD">
          <i class="fas fa-fw fa-cog"></i>
          <span>In Out Records</span>
        </a>
        <div id="collapseIOD" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white collapse-inner rounded">
            <a class="collapse-item" href="browse-receipts.php">Receipts</a>
            <a class="collapse-item" href="browse-orders.php">Orders</a>
            <a class="collapse-item" href="browse-inbounds.php">Inbounds</a>
            <a class="collapse-item" href="browse-outbounds.php">Outbounds</a>
            <!-- <a class="collapse-item" href="browse-relocations.php">Relocations</a> -->
          </div>
        </div>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">



      <!-- Show Logging In User & Sign Out Button-->
      <li class="text-white" style="padding-top: 10px; padding-bottom: 5px; margin-left: 5px;" data-toggle="#" data-target="#" aria-expanded="true" aria-controls="#">

        <button href="javascript://" onclick="self.parent.location='logout.php'" button type="button" class="btn btn-primary">
          <span class="icon text-white-50">
            <i class="fa fa-sign-out" aria-hidden="true"></i>
          </span>
          <span class="text" style="margin-left: 5px">Sign Out</span>
        </button>
      </li>
      <!-- Version -->
      <li>
        <div class="container text-gray-300 text-sm-center font-weight-lighter">
          <small>v <?php echo $db->GetCurrentVersion() ?></small>
        </div>
      </li>
    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <?php
      $AllData = $db->getInStockSummary();
      ?>

      <div class="container-fluid" id="content">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h2 class="h3 mb-0 text-gray-800" style="padding-top: 20px;padding-bottom: 10px;">Welcome to Starbeacon Inventory System!</h1>
        </div>

        <div class="row">
          <div class="col-xl-4 col-md-8 mb-6">
            <div class="card border-left-primary shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-3">
                    <div class="text-xl font-weight-bold text-gray">Stock Summary</div>
                    <table class="table table-striped table-bordered">
                      <thead>
                        <tr class="bg-primary text-white">
                          <th>Item Name</th>
                          <th align="right">Qty</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $s  =  '';
                        foreach ($AllData as $val) {
                          $s++;
                        ?>
                          <tr>
                            <td><?php echo $val['item_name']; ?></td>
                            <td align="right"><?php echo $val['Qty']; ?></td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-md-8 mb-6">
            <div class="card border-left-success shadow py-2">
              <div class="card-body">

                <div class="row no-gutters align-items-center">
                  <div class="col mr-3">
                    <div class="text-xl font-weight-bold text-gray">Reports</div>
                    <br><a href="in-stock_items.php"><b>In-Stock Items List</b></a>
                    <br><a href="all_inbound_items_records.php"><b>All Inbound Items Records</b></a>
                    <br><a href="all_outbound_items_records.php"><b>All Outbound Items Records</b></a>
                    <br><a href="full_transaction_records.php"><b>Full Transaction Records</b></a>
                  </div>
                </div>        
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; starbeacon 2019 (v<?php echo $db->GetCurrentVersion() ?>)</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

</body>

</html>