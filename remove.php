<?php
include_once('config.php');

session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
	header("location: login.php");
	exit;
}

if (isset($_REQUEST['delId']) and $_REQUEST['delId'] != "") {
	$db->delete($_REQUEST['target'], array('id' => $_REQUEST['delId']));


	//update item histories if not remove from inbound
	if ($_REQUEST['target'] == "inbound_details") {
		$data2	=	array(
			'inbound_detail_id' => $_REQUEST['delId'],
			'status' => 'Inbound Cancel',
			'updated_by_user_id' => $_SESSION["id"]
		);
		$db->insert('item_histories', $data2);
	} else if ($_REQUEST['target'] == "outbound_details") {
		$data2	=	array(
			'inbound_detail_id' => $_REQUEST['inbound_detail_id'],
			'status' => 'Outbound Cancel',
			'updated_by_user_id' => $_SESSION["id"]
		);
		$db->insert('item_histories', $data2);
	} else if ($_REQUEST['target'] != "inbound_details") {
		$data2	=	array(
			'inbound_detail_id' => $_REQUEST['delId'],
			'status' => 'Deleted',
			'updated_by_user_id' => $_SESSION["id"]
		);
		$db->insert('item_histories', $data2);
	}

	if (isset($_REQUEST['rid']) and $_REQUEST['rid'] != "") {
		header('location: browse-' . $_REQUEST['target'] . '.php?rid=' . $_REQUEST['rid'] . '&msg=rrs');
	} else {
		header('location: browse-' . $_REQUEST['target'] . '.php?msg=rna');
	}
	exit;
}
