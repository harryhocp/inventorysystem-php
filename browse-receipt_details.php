<?php include_once('config.php');
$table = "receipt_details";

if (isset($_REQUEST['rid']) and $_REQUEST['rid'] != "") {
	$receipt_id = $_REQUEST['rid'];
}
?>
<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
	header("location: login.php");
	exit;
}
?>
<!doctype html>
<html lang="en-US" xmlns:fb="https://www.facebook.com/2008/fbml" xmlns:addthis="https://www.addthis.com/help/api-spec" prefix="og: http://ogp.me/ns#" class="no-js">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>starbeacon - Inventory System</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<script src="https://use.fontawesome.com/4102c26c2b.js"></script>

	<!-- Custom fonts for this template-->
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">



		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<?php
				$condition	=	'';
				if (isset($_REQUEST['rid']) and $_REQUEST['rid'] != "") {
					$condition	.=	' AND receipt_id LIKE "%' . $_REQUEST['rid'] . '%" ';
				}
				$AllData = $db->getReceiptDetails($table, $condition, 'ORDER BY a.id');
				$receipt_no = $db->getReceiptNo($receipt_id);
				$userList = $db->getAllUsernames();


				$updated_by_user_id = "all";
				if (isset($_REQUEST['updated_by_user_id']) && !is_null($_REQUEST['updated_by_user_id'])) $updated_by_user_id = $_REQUEST['updated_by_user_id'];

				?>



				<div class="container-fluid">
					<h1 class="h3 mb-0 text-gray-800" style="padding-top: 20px;padding-bottom: 10px;">Receipt Details</h1>
					<div class="card">
						<div class="card-header"><i class="fa fa-fw fa-globe"></i> <strong>Browse Receipt Details</strong><a href="add-<?php echo $table; ?>.php?rid=<?php echo $receipt_id; ?>" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-plus-circle"></i> Add Receipt Details</a></div>
						<div class="card-body">
							<?php
							if (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "rds") {
								echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Record status updated to inactive successfully!</div>';
							} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "rac") {
								echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Record status updated to active successfully!</div>';
							} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "rus") {
								echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Record updated successfully!</div>';
							} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "rnu") {
								echo	'<div class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> You did not change any thing!</div>';
							} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "rna") {
								echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> There is some thing wrong <strong>Please try again!</strong></div>';
							} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "ras") {
								echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Record added successfully!</div>';
							} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "rrs") {
								echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Record removed successfully!</div>';
							}
							?>
						</div>
					</div>
					<hr>
					<div><strong>Receipt No: <?php echo $receipt_no; ?></strong></div>


					<!-- Display Data Section -->
					<div>
						<table class="table table-striped table-bordered">
							<thead>
								<tr class="bg-primary text-white">
									<th>ID</th>
									<th>Item Name</th>
									<th>Unit Price</th>
									<th>Qty</th>
									<th>Last Updated By</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$s	=	'';
								foreach ($AllData as $val) {
									$s++;
									?>
									<tr>
										<td><?php echo $val['id']; ?></td>
										<td><?php echo $val['item_name']; ?></a></td>
										<td><?php echo $val['unit_price']; ?></td>
										<td><?php echo $val['qty']; ?></td>
										<td><?php echo $val['user_name']; ?></td>
										<td align="center">
											<a href="edit-<?php echo $table ?>.php?&rid=<?php echo $receipt_id; ?>&editId=<?php echo $val['id']; ?>" class="text-primary"><i class="fa fa-fw fa-edit"></i> Edit</a> |
											<a href="remove.php?delId=<?php echo $val['id']; ?>&target=<?php echo $table; ?>&rid=<?php echo $receipt_id; ?>" class="text-danger" onClick="return confirm('Are you sure to remove this item?');"><i class="fa fa-fw fa-times-circle"></i> Remove</a>
	
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<!--/.col-sm-12-->

				</div>
			</div>
			<!-- End of Main Content -->

			<!-- Footer -->
			<footer class="sticky-footer bg-white">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Copyright &copy; starbeacon 2019</span>
					</div>
				</div>
			</footer>
			<!-- End of Footer -->

		</div>
		<!-- End of Content Wrapper -->

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

</body>

</html>