<?php include_once('config.php');
$table = "items";
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
	header("location: login.php");
	exit;
}
?>
<!doctype html>
<html lang="en-US">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>starbeacon - Inventory System</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<script src="https://use.fontawesome.com/4102c26c2b.js"></script>

	<!-- Custom fonts for this template-->
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

		<!-- Sidebar -->
		<ul class="navbar-nav bg-primary sidebar sidebar-dark accordion" id="accordionSidebar">

			<!-- Sidebar - Brand -->
			<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
				<img src="img/starbeacon_logo.png" width="100%" height="46" alt="" />
			</a>

			<!-- Title -->
			<li class="text-white text-center text-lg" style="padding-top: 10px; padding-bottom: 10px;" data-toggle="#" data-target="#" aria-expanded="true" aria-controls="#">
				<span>Inventory System</span>
			</li>
			<!-- Divider -->
			<hr class="sidebar-divider my-0">


			<li class="text-white text-left" data-toggle="#" data-target="#" aria-expanded="true" aria-controls="#">
				<i class="fa fa-user" style="margin-left: 20px; margin-right: 10px;padding-top: 18px; padding-bottom: 18px;"></i>
				<?php echo htmlspecialchars($_SESSION["username"]); ?>
			</li>


			<!-- Divider -->
			<hr class="sidebar-divider my-0">

			<!-- Master Records Drop Down-->
			<li class="nav-item active">
				<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseMD" aria-expanded="true" aria-controls="collapseMD">
					<i class="fas fa-fw fa-cog"></i>
					<span>Master Records</span>
				</a>
				<div id="collapseMD" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
					<div class="bg-white collapse-inner rounded">
						<h6 class="collapse-header">Main:</h6>
						<a class="collapse-item active" href="browse-items.php"><b>Items</a>
						<a class="collapse-item" href="browse-locations.php">Locations</a>
						<a class="collapse-item" href="browse-customers.php">Customers</a>
<a class="collapse-item" href="browse-projects.php">Projects</a>
						<a class="collapse-item" href="browse-users.php">Users</a></b>						<h6 class="collapse-header">Items:</h6>
						<a class="collapse-item" href="browse-vendors.php">Vendors</a>
						<a class="collapse-item" href="browse-colors.php">Colors</a>
						<a class="collapse-item" href="browse-enclosures.php">Enclosures</a>
						<a class="collapse-item" href="browse-firmwares.php">Firmwares</a>
						<a class="collapse-item" href="browse-item_types.php">Item Types</a>
					</div>
				</div>
			</li>

			<!-- Divider -->
			<hr class="sidebar-divider my-0">
			<!-- In-Out Records Drop Down-->
			<li class="nav-item">
				<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseIOD" aria-expanded="true" aria-controls="collapseIOD">
					<i class="fas fa-fw fa-cog"></i>
					<span>In Out Records</span>
				</a>
				<div id="collapseIOD" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
					<div class="bg-white collapse-inner rounded">
						<a class="collapse-item" href="browse-receipts.php">Receipts</a>
						<a class="collapse-item" href="browse-orders.php">Orders</a>
						<a class="collapse-item" href="browse-inbounds.php">Inbounds</a>
						<a class="collapse-item" href="browse-outbounds.php">Outbounds</a>
						<!-- <a class="collapse-item" href="browse-relocations.php">Relocations</a> -->
					</div>
				</div>
			</li>
			<!-- Divider -->
			<hr class="sidebar-divider my-0">



			<!-- Show Logging In User & Sign Out Button-->
			<li class="text-white" style="padding-top: 10px; padding-bottom: 5px; margin-left: 5px;" data-toggle="#" data-target="#" aria-expanded="true" aria-controls="#">

				<button href="javascript://" onclick="self.parent.location='logout.php'" button type="button" class="btn btn-primary">
					<span class="icon text-white-50">
						<i class="fa fa-sign-out" aria-hidden="true"></i>
					</span>
					<span class="text" style="margin-left: 5px">Sign Out</span>
				</button>
			</li>
			<!-- Version -->
			<li>
				<div class="container text-gray-300 text-sm-center font-weight-lighter">
					<small>v <?php echo $db->GetCurrentVersion() ?></small>
				</div>
			</li>
		</ul>
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<?php
				$condition	=	'';
				if (isset($_REQUEST['vname']) and $_REQUEST['vname'] != "") {
					$condition	.=	' AND vname LIKE "%' . $_REQUEST['vname'] . '%" ';
				}
				if (isset($_REQUEST['icode']) and $_REQUEST['icode'] != "") {
					$condition	.=	' AND icode LIKE "%' . $_REQUEST['icode'] . '%" ';
				}
				if (isset($_REQUEST['iname']) and $_REQUEST['iname'] != "") {
					$condition	.=	' AND iname LIKE "%' . $_REQUEST['iname'] . '%" ';
				}
				if (isset($_REQUEST['itname']) and $_REQUEST['itname'] != "") {
					$condition	.=	' AND itname LIKE "%' . $_REQUEST['itname'] . '%" ';
				}
				if (isset($_REQUEST['imodel_no']) and $_REQUEST['imodel_no'] != "") {
					$condition	.=	' AND model_no LIKE "%' . $_REQUEST['imodel_no'] . '%" ';
				}
				if (isset($_REQUEST['iremarks']) and $_REQUEST['iremarks'] != "") {
					$condition	.=	' AND iremarks LIKE "%' . $_REQUEST['iremarks'] . '%" ';
				}
				if (isset($_REQUEST['cname']) and $_REQUEST['cname'] != "") {
					if ($_REQUEST['cname'] != "all") {
						$condition	.=	' AND c.id LIKE "%' . $_REQUEST['cname'] . '%" ';
					}
				}
				if (isset($_REQUEST['etype']) and $_REQUEST['etype'] != "") {
					if ($_REQUEST['etype'] != "all") {
						$condition	.=	' AND e.id LIKE "%' . $_REQUEST['etype'] . '%" ';
					}
				}
				if (isset($_REQUEST['fversion']) and $_REQUEST['fversion'] != "") {
					$condition	.=	' AND fversion LIKE "%' . $_REQUEST['fversion'] . '%" ';
				}
				if (isset($_REQUEST['istatus']) and $_REQUEST['istatus'] != "") {
					if ($_REQUEST['istatus'] != "all") {
						$condition	.=	' AND status LIKE "%' . $_REQUEST['istatus'] . '%" ';
					}
				}
				if (isset($_REQUEST['updated_by_user_id']) and $_REQUEST['updated_by_user_id'] != "") {
					if ($_REQUEST['updated_by_user_id'] != "all") {
						$condition	.=	' AND updated_by_user_id LIKE "%' . $_REQUEST['updated_by_user_id'] . '%" ';
					}
				}
				$AllData = $db->getItemsRecords($table, $condition, 'ORDER BY i.id');
				$userList = $db->getAllUsernames();
				$colorList = $db->getAllColors();
				$enclosureList = $db->getAllEnclosureTypes();
				$status = "all";
				if (isset($_REQUEST['status']) && !is_null($_REQUEST['status'])) $status = $_REQUEST['status'];
				$cname = "all";
				if (isset($_REQUEST['cname']) && !is_null($_REQUEST['cname'])) $cname = $_REQUEST['cname'];
				$etype = "all";
				if (isset($_REQUEST['etype']) && !is_null($_REQUEST['etype'])) $etype = $_REQUEST['etype'];
				$updated_by_user_id = "all";
				if (isset($_REQUEST['updated_by_user_id']) && !is_null($_REQUEST['updated_by_user_id'])) $updated_by_user_id = $_REQUEST['updated_by_user_id'];

				?>

				<!-- Content Wrapper -->
				<div id="content-wrapper" class="d-flex flex-column">

					<!-- Main Content -->
					<div id="content">

						<div class="container-fluid">
							<h1 class="h3 mb-0 text-gray-800" style="padding-top: 20px;padding-bottom: 10px;"><?php echo ucfirst($table) ?></h1>
							<div class="card">
								<div class="card-header"><i class="fa fa-fw fa-globe"></i> <strong>Browse <?php echo ucfirst(substr($table, 0, -1)) ?></strong> <a href="add-<?php echo $table; ?>.php" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-plus-circle"></i> Add <?php echo ucfirst($table) ?></a></div>
								<div class="card-body">
									<?php
									if (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "rds") {
										echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Record status updated to inactive successfully!</div>';
									} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "ras") {
										echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Record status updated to active successfully!</div>';
									} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "rus") {
										echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Record updated successfully!</div>';
									} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "rnu") {
										echo	'<div class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> You did not change any thing!</div>';
									} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "rna") {
										echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> There is some thing wrong <strong>Please try again!</strong></div>';
									} elseif (isset($_REQUEST['msg']) and $_REQUEST['msg'] == "rad") {
										echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Record added successfully!</div>';
									}
									?>
									<div class="col-sm-12">
										<h5 class="card-title"><i class="fa fa-fw fa-search"></i> Find <?php echo ucfirst(substr($table, 0, -1)) ?></h5>
										<form method="get">
											<div class="row">
												<div class="col-md-4 col-lg-2">
													<div class="form-group">
														<label>Vendor Name</label>
														<input type="text" name="vname" id="vname" class="form-control" value="<?php echo isset($_REQUEST['vname']) ? $_REQUEST['vname'] : '' ?>" placeholder="Enter vendor name">
													</div>
												</div>
												<div class="col-md-4 col-lg-2">
													<div class="form-group">
														<label>Item Code</label>
														<input type="text" name="icode" id="icode" class="form-control" value="<?php echo isset($_REQUEST['icode']) ? $_REQUEST['icode'] : '' ?>" placeholder="Enter item code">
													</div>
												</div>
												<div class="col-md-4 col-lg-2">
													<div class="form-group">
														<label>Item Name</label>
														<input type="text" name="iname" id="iname" class="form-control" value="<?php echo isset($_REQUEST['iname']) ? $_REQUEST['iname'] : '' ?>" placeholder="Enter item name">
													</div>
												</div>

												<div class="col-md-4 col-lg-2">
													<div class="form-group">
														<label>Item Type</label>
														<input type="text" name="itname" id="itname" class="form-control" value="<?php echo isset($_REQUEST['itname']) ? $_REQUEST['itname'] : '' ?>" placeholder="Enter item type">
													</div>
												</div>
												<div class="col-md-4 col-lg-2">
													<div class="form-group">
														<label>Model No</label>
														<input type="text" name="imodelno" id="imodelno" class="form-control" value="<?php echo isset($_REQUEST['imodelno']) ? $_REQUEST['imodelno'] : '' ?>" placeholder="Enter model no">
													</div>
												</div>
												<div class="col-md-4 col-lg-2">
													<div class="form-group">
														<label>Color</label>
														<select name="cname" id="cname" class="form-control">
															<option value="all" <?= ($cname == "all" ? 'selected' : '') ?>>All</option>
															<?php
															$s	=	'';
															foreach ($colorList as $val) {
																$s++;
																?>
																<?php
																$color_selected = "";
																if ($cname == $val['cid'])
																	$color_selected = "selected";
																echo "<option value='" . $val['cid'] . "' " . $color_selected . ">" . $val['cname'] . "</option>" ?>
															<?php } ?>
														</select>
													</div>
												</div>
												<div class="col-md-4 col-lg-2">
													<div class="form-group">
														<label>Enclosure Type</label>
														<select name="etype" id="etype" class="form-control">
															<option value="all" <?= ($etype == "all" ? 'selected' : '') ?>>All</option>
															<?php
															$s	=	'';
															foreach ($enclosureList as $val) {
																$s++;
																?>
																<?php
																$enclosure_selected = "";
																if ($etype == $val['eid'])
																	$enclosure_selected = "selected";
																echo "<option value='" . $val['eid'] . "' " . $enclosure_selected . ">" . $val['etype'] . "</option>" ?>
															<?php } ?>
														</select>
													</div>
												</div>
												<div class="col-md-4 col-lg-2">
													<div class="form-group">
														<label>Firmware Version</label>
														<input type="text" name="fversion" id="fversion" class="form-control" value="<?php echo isset($_REQUEST['fversion']) ? $_REQUEST['fversion'] : '' ?>" placeholder="Enter firmware version">
													</div>
												</div>
												<div class="col-md-4 col-lg-2">
													<div class="form-group">
														<label>Remarks</label>
														<input type="text" name="remarks" id="remarks" class="form-control" value="<?php echo isset($_REQUEST['remarks']) ? $_REQUEST['remarks'] : '' ?>" placeholder="Enter remarks">
													</div>
												</div>
												<div class="col-md-4 col-lg-2">
													<div class="form-group">
														<label>Status</label>
														<select name="status" id="status" class="form-control">
															<option value="all" <?= ($status == "all" ? 'selected' : '') ?>>All</option>
															<option value="1" <?= ($status == "1" ? 'selected' : '') ?>>Active</option>
															<option value="0" <?= ($status == "0"  ? 'selected' : '') ?>>Inactive</option>
														</select>
													</div>
												</div>
												<div class="col-md-4 col-lg-2">
													<div class="form-group">
														<label>Last Updated By</label>
														<select name="updated_by_user_id" id="updated_by_user_id" class="form-control">
															<option value="all" <?= ($updated_by_user_id == "all" ? 'selected' : '') ?>>All</option>
															<?php
															$s	=	'';
															foreach ($userList as $val) {
																$s++;
																?>
																<?php
																$user_selected = "";
																if ($updated_by_user_id == $val['id'])
																	$user_selected = "selected";
																echo "<option value='" . $val['id'] . "' " . $user_selected . ">" . $val['user_name'] . "</option>" ?>
															<?php } ?>
														</select>
													</div>
												</div>
												<!-- Search and Clear Button-->
												<div class="col-md-4 col-lg-2">
													<div class="rows">
														<div class="col-sm-16">
															<div class="form-group">
																<label>&nbsp;</label>
																<div class="text-right">
																	<button style="min-width:100px" type="submit" name="submit" value="search" id="submit" class="btn bg-success text-white"><i class="fa fa-fw fa-search"></i> Search</button>
																	<a style="min-width:100px" href="browse-<?php echo $table ?>.php" class="btn btn-danger "><i class="fa fa-fw fa-sync"></i> Clear</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
							<hr>


							<!-- Display Data Section -->
							<div class="table-responsive">
								<table class="table table-striped table-bordered w-auto">
									<thead>
										<tr class="bg-primary text-white">
											<th>ID</th>
											<th>Vendor Name</th>
											<th>Item Code</th>
											<th>Item Name</th>
											<th>Item Type Name</th>
											<th>Model No</th>
											<th>Color</th>
											<th>Enclosure Type</th>
											<th>Firmware Version</th>
											<th>Remarks</th>
											<th>Status</th>
											<th>Last Updated By</th>
											<th class="text-center">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$s	=	'';
										foreach ($AllData as $val) {
											$s++;
											?>
											<tr>
												<td><?php echo $val['iid']; ?></td>
												<td><?php echo $val['vname']; ?></td>
												<td><?php echo $val['icode']; ?></td>
												<td><?php echo $val['iname']; ?></td>
												<td><?php echo $val['itname']; ?></td>
												<td><?php echo $val['imodelno']; ?></td>
												<td><?php echo $val['cname']; ?></td>
												<td><?php echo $val['etype']; ?></td>
												<td><?php echo $val['fversion']; ?></td>
												<td><?php echo $val['iremarks']; ?></td>
												<td><?php echo $val['istatus']; ?></td>
												<td><?php echo $val['user_name']; ?></td>
												<td align="center">
													<a href="edit-<?php echo $table ?>.php?editId=<?php echo $val['iid']; ?>" class="text-primary"><i class="fa fa-fw fa-edit"></i> Edit</a> |
													<?php
													if ($val['istatus'] == "Active") { ?>
														<a href="delete.php?Id=<?php echo $val['iid']; ?>&target=<?php echo $table; ?>&action=deactivate" class="text-danger" onClick="return confirm('Are you sure to deactivate this <?php echo substr($table, 0, -1) ?>?');"><i class="fa fa-fw fa-times-circle"></i> Deactivate</a>
													<? } else { ?>
														<a href="delete.php?Id=<?php echo $val['iid']; ?>&target=<?php echo $table; ?>&action=activate" class="text-success" onClick="return confirm('Are you sure to activate this <?php echo substr($table, 0, -1) ?>?');"><i class="fa fa-fw fa-check-circle"></i> Activate</a>
													<? } ?>
												</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
							<!--/.col-sm-12-->

						</div>
					</div>
					<!-- End of Main Content -->

					<!-- Footer -->
					<footer class="sticky-footer bg-white">
						<div class="container my-auto">
							<div class="copyright text-center my-auto">
								<span>Copyright &copy; starbeacon 2019</span>
							</div>
						</div>
					</footer>
					<!-- End of Footer -->

				</div>
				<!-- End of Content Wrapper -->
			</div>
		</div>
	</div>
</body>

</html>