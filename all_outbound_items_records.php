<?php
// Initialize the session
session_start();
include_once('config.php');
// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
  header("location: login.php");
  exit;
}


// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=All_Outbound_Items_'. date("Ymd") .'.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
fputcsv($output, array('Order No', 'Order Date', 'Item Type', 'Color', 'Enclosure Type', 'Serial Number', 'Qty', 
'Box No', 'Location Code','Vendor Name', 'Action', 'Delivery Date','Order No', 'Record Time', 'Updated By'));

// // fetch the data

$data = $db->getAllOutboundItems();

// // loop over the rows, outputting them
//while ($row = mysql_fetch_assoc($rows)) 

foreach ($data as $rows){
fputcsv($output, $rows);
}
?>